# Suchmaschinentechnologie Übung 1

## Aufgabe

Erstellen Sie einen eigenen Web-Crawler und dokumentieren Sie die Implementierung in dieser Datei.

# Hinweis

Sie sollten die Library *jsoup* verwenden und diese daher in die Datei *gradle.build* oder *pom.xml* eintragen.
Ob sie *Gradle* oder *Maven* verwenden, bleibt Ihnen frei gestellt.
